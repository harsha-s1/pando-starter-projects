const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
  entry: "./src/root.tsx",
  devtool: "inline-source-map",

  resolve: {
    extensions: [".tsx", ".ts", ".js", ".mjs", ".cjs"],
    alias: {
      "@/styled-system/patterns": path.resolve(
        __dirname,
        "./styled-system/patterns",
      ),
      "@/styled-system/css": path.resolve(__dirname, "./styled-system/css"),
      "@/styled-system/recipes": path.resolve(
        __dirname,
        "./styled-system/recipes",
      ),
    },
  },

  module: {
    rules: [
      {
        test: /\.(?:js|mjs|cjs|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            plugins: [],
            presets: [
              "@babel/preset-env",
              [
                "@babel/preset-react",
                {
                  runtime: "automatic",
                },
              ],
              "@babel/preset-typescript",
            ],
            include: ["src/**/*"],
            ignore: ["**/*.d.ts"],
          },
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: "public/index.html",
    }),
  ],

  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "build"),
    clean: true,
  },
};
