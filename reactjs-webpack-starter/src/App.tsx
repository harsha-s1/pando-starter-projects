import { Button, createButtonIconProps } from '@pluralsight/react';
import { PlaceholderIcon } from '@pluralsight/react/icons';
import { css } from "@/styled-system/css";
import { vstack } from "@/styled-system/patterns";
import { button } from "@/styled-system/recipes";

export function App() {
  return (
    <div
      className={vstack({
        justify: 'center',
        height: "100dvh",
        width: "100dvw",
      })}
    >
      <h1 className={css({
        textStyle: "h1",
      })}>App</h1>
      <button className={button()}>Test</button>

      <div className={css({ mt: '4' })}>
        <Button startIcon={<PlaceholderIcon {...createButtonIconProps()} />} usage="outline">React Button</Button>
      </div>
    </div>
  );
}
