import { createRoot } from "react-dom/client";
import { StrictMode } from "react";

import '@pluralsight/panda-preset/pando.css'
import "./root.css";

import { App } from "./App";

const root = createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <StrictMode>
    <App />
  </StrictMode>,
);
