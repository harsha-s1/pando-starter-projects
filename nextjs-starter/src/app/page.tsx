import { Button, FormControlProvider, Radio } from "@pluralsight/react";
import { css } from "@/styled-system/css";
import { container } from "@/styled-system/patterns";
import { button } from "@/styled-system/recipes";

export default function Home() {
  return (
    <div className={container()}>
      <Button size="md" usage="text">
        I am Pandos React Button component!
      </Button>
      <h1 className={css({ textStyle: "h1", color: "neutral.text.100" })}>
        Pando design system
      </h1>
      <button className={button()}>I am Pandos button recipe!</button>

      <section className={css({ my: "4" })}>
        <h3>React Usage</h3>
        <fieldset name="react-option">
          <legend>Choose an option</legend>
          <FormControlProvider readOnly>
            <Radio defaultChecked={true} name="readonly">
              Read only
            </Radio>
          </FormControlProvider>
          <FormControlProvider disabled>
            <Radio defaultChecked={false} name="disabled">
              Disabled
            </Radio>
          </FormControlProvider>
          <FormControlProvider>
            <Radio
              name="normal"
              // onChange={() => {console.log('test')}}
              // checked={true}
              value="normal"
            >
              Normal
            </Radio>
          </FormControlProvider>
          <FormControlProvider invalid>
            <Radio
              name="invalid"
              // onChange={() => {console.log('test')}}
              // checked={true}
              value="invalid"
            >
              Invalid
            </Radio>
          </FormControlProvider>
        </fieldset>
      </section>
    </div>
  );
}
