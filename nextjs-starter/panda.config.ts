import { defineConfig } from "@pandacss/dev";
import pandaPreset from "@pandacss/preset-panda";
import pandoPreset, { pandoConfig } from "@pluralsight/panda-preset";

export default defineConfig({
  ...pandoConfig,

  include: [
    "./src/components/**/*.{ts,tsx,js,jsx}",
    "./src/app/**/*.{ts,tsx,js,jsx}",
  ],
  exclude: [],

  presets: [pandaPreset, pandoPreset],
});
