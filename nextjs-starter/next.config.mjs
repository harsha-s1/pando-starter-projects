import { fileURLToPath } from "url";
import path from "path";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const nextConfig = {};

const webpack = (config, {}) => {
  config.resolve.alias = {
    ...config.resolve.alias,
    "@": __dirname,
    "@/*": path.resolve(__dirname, "*"),
  };
  return config;
};

export default {
  ...nextConfig,
  webpack,
};
