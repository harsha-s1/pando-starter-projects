import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
// https://vitejs.dev/config/
export default defineConfig({
  resolve:{
  alias: {
    '@/styled-system/patterns': path.resolve(__dirname, './styled-system/patterns'),
    '@/styled-system/css': path.resolve(__dirname, './styled-system/css'),
    '@/styled-system/recipes': path.resolve(__dirname, './styled-system/recipes'),
  },
  },
  plugins: [react()],
})
